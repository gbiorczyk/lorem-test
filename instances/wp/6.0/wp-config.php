<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv( 'DB_USER' ) );

/** Database username */
define( 'DB_USER', getenv( 'DB_USER' ) );

/** Database password */
define( 'DB_PASSWORD', getenv( 'DB_USER' ) );

/** Database hostname */
define( 'DB_HOST', getenv( 'DB_HOST' ) );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'GXQ#~HgF&g-%Gz=.9R7~3;hj^aS@T.(Fqlsy<jl(xi$[:.nFS/)_~x2-&D!dQ/4b' );
define( 'SECURE_AUTH_KEY',  '`-xPUuia_$m][?>nG]+{E!xG^U+oVr/y=d!+zFhY-S/5vy0dq5iO{f9W^c.i+/-Z' );
define( 'LOGGED_IN_KEY',    '%F}RuJ(2F;Tv}aO_4=p-IMe|3KLJLt{IzZ-NSD=(e|sVDCL|s?3p[=7+iW^o|.&W' );
define( 'NONCE_KEY',        '+yz&{{-7|3Zh7+d&|{,~TqB=icvh-CsAwKJK/Fi7AGf4/uIc^Wi#Da6a#WK!9ihm' );
define( 'AUTH_SALT',        'vQ1@,ciFE+6D= *3(Fomm%24OV/G6)$|Jx`A_O@-ShV>W-)Mf82u~rQVU1HHjzY{' );
define( 'SECURE_AUTH_SALT', '&Uv`-=:l.^]sYXW?jgCK,|8A#O=V-)8}6d$u<NeS#X{8wP|WEBX#Yx+DUV3G-jJa' );
define( 'LOGGED_IN_SALT',   '^do}]*PH|%Fp%Q<4,b|&r|`DpfZQ$9me;+wg 563Nn6WpA7S:a[5eHZw/eSh|EcH' );
define( 'NONCE_SALT',       '&B35pbiJc#:&a]<V*>,LKxC|9b}>R^!?GOn:Pn137J;8o:MrsX7:{&~b`AnX?tjf' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp33_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', true );
define( 'SCRIPT_DEBUG', true );
define( 'FS_METHOD', 'direct' );

define( 'WP_MEMORY_LIMIT', '312M' );
define( 'WC_LOG_HANDLER', 'WC_Log_Handler_DB' );

define( 'WP_AUTO_UPDATE_CORE', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
