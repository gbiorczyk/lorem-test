# Adding new user

1. Use the commands:

```
useradd -m rangers
adduser rangers www-data
passwd rangers

chsh -s /bin/bash rangers

sudo su - rangers
mkdir www
```

2. Generate the SSH key:

```
ssh-keygen
cat ~/.ssh/id_rsa.pub
```

3. Add the key to your GitLab account.

# Composer installation

1. Use the commands:

```
add-apt-repository ppa:ondrej/php
apt-get update && apt-get upgrade
apt-get install php7.4-cli php7.4-curl php7.4-dom php7.4-zip php7.4-mbstring
```

2. Install Composer via [command-line](https://getcomposer.org/download/).
3. Make Composer globally available:

```
mv composer.phar /usr/bin/composer
```

# Node.js installation

1. Use the commands:

```
apt-get install npm
```

# Environment management:

1. Environment initialization:

```
mkdir /opt/rangers
cd /opt/rangers
sh ./start.sh
```

2. Updating the plugin (when logged in as rangers user):

```
cd /plugins/flexible-checkout-fields && git reset --hard
git checkout devel && git pull origin devel
composer install && composer install --no-dev
```
