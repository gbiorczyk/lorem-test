#!/bin/bash

chmod +x ./env.sh
. ./env.sh

chmod +x ./stop.sh
./stop.sh

docker-compose up -d

containers=$(sudo docker ps --filter status=running | awk '{if(NR>1) print $NF}')
for container in $containers; do
	if [ -d /home/"${SYSTEM_USER}"/www/"$container"/ ]; then
		chown -R ${SYSTEM_USER}:www-data /home/"${SYSTEM_USER}"/www/"$container"/
		rm /home/"${SYSTEM_USER}"/www/"$container"/debug.log

		source_instance=$(docker exec "$container" bash -c 'echo "$SOURCE_INSTANCE"')
		if [ "$source_instance" != "" ] && ! [ -f /home/"${SYSTEM_USER}"/www/"$container"/index.php ]; then
			docker cp "$source_instance"/wp-content/. "$container":/var/www/html/wp-content
		fi

		chown -R ${SYSTEM_USER}:www-data /home/"${SYSTEM_USER}"/www/"$container"/
        setfacl -R -b /home/"${SYSTEM_USER}"/www/"$container"/
        setfacl -Rdm u:${SYSTEM_USER}:rwx,g:www-data:rwx,o:0 /home/"${SYSTEM_USER}"/www/"$container"/
        setfacl -Rm u:${SYSTEM_USER}:rwx,g:www-data:rwx,o:0 /home/"${SYSTEM_USER}"/www/"$container"/
	fi
done

chown -R ${SYSTEM_USER}:www-data /home/"${SYSTEM_USER}"/plugins/
